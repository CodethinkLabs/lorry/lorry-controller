Run queue management
====================

This chapter contains tests meant for managing the run-queue in
WEBAPP.

Start and stop job scheduling
-----------------------------

The administrator needs to be able to stop WEBAPP from scheduling any
new jobs, and later to start it again.

    SCENARIO admin can start and stop WEBAPP job scheduling
    GIVEN a running WEBAPP
    WHEN admin makes request GET /1.0/status
    THEN response has running_queue set to true

    WHEN admin makes request POST /1.0/stop-queue
    AND admin makes request GET /1.0/status
    THEN response has running_queue set to false

Further, the state change needs to be persistent across WEBAPP
instances, so we kill the WEBAPP that's currently running, and start a
new one, and verify that the `running-queue` status is still `true`.

    WHEN WEBAPP is terminated
    THEN WEBAPP isn't running

    WHEN WEBAPP is started
    WHEN admin makes request GET /1.0/status
    THEN response has running_queue set to false

Start the queue again.

    WHEN admin makes request POST /1.0/start-queue
    AND admin makes request GET /1.0/status
    THEN response has running_queue set to true

Finally, clean up.

    FINALLY WEBAPP terminates


Read CONFGIT
------------

We need to be able to get Lorry Controller, specifically WEBAPP, to
update its configuration and run-queue from CONFGIT using the
`/1.0/read-configuration` HTTP API request.

First, set up WEBAPP.

    SCENARIO WEBAPP updates its configuration from CONFGIT
    GIVEN a new git repository in CONFGIT
    AND WEBAPP uses CONFGIT as its configuration directory
    AND a running WEBAPP

We'll start with an empty configuration. This is the default state
when WEBAPP has never read its configuration.

    WHEN admin makes request GET /1.0/list-queue
    THEN response has queue set to []

Make WEBAPP read an empty configuration. Or rather, a configuration
that does not match any existing `.lorry` files.

    GIVEN an empty lorry-controller.conf in CONFGIT
    WHEN admin makes request POST /1.0/read-configuration
    AND admin makes request GET /1.0/list-queue
    THEN response has queue set to []

Add a `.lorry` file, with one Lorry spec, and make sure reading the
configuration makes `/list-queue` report it.

    GIVEN Lorry file CONFGIT/foo.lorry with {"foo":{"type":"git","url":"git://foo"}}
    AND lorry-controller.conf in CONFGIT adds lorries *.lorry using prefix upstream
    WHEN admin makes request POST /1.0/read-configuration
    AND admin makes request GET /1.0/list-queue
    THEN response has queue set to ["upstream/foo"]

If the `.lorry` file is removed, the queue should again become empty.

    GIVEN file CONFGIT/foo.lorry is removed
    WHEN admin makes request POST /1.0/read-configuration
    AND admin makes request GET /1.0/list-queue
    THEN response has queue set to []

Add two Lorries, then make sure they can reordered at will.

    GIVEN Lorry file CONFGIT/foo.lorry with {"foo":{"type":"git","url":"git://foo"}}
    AND Lorry file CONFGIT/bar.lorry with {"bar":{"type":"git","url":"git://bar"}}
    WHEN admin makes request POST /1.0/read-configuration
    AND admin makes request GET /1.0/list-queue
    THEN response has queue set to ["upstream/bar", "upstream/foo"]

    WHEN admin makes request POST /1.0/move-to-top with path=upstream/foo
    AND admin makes request GET /1.0/list-queue
    THEN response has queue set to ["upstream/foo", "upstream/bar"]

    WHEN admin makes request POST /1.0/move-to-bottom with path=upstream/foo
    AND admin makes request GET /1.0/list-queue
    THEN response has queue set to ["upstream/bar", "upstream/foo"]

If trying to move a lorry that doesn't exist, make sure there's an
appropriate error message.

    WHEN admin makes request POST /1.0/move-to-bottom with path=upstream/alfred
    THEN response matches "upstream/alfred does not exist"

Likewise, if we forget to give a path argument, there should be an
error message.

    WHEN admin makes request POST /1.0/move-to-bottom
    THEN response matches "path.*not given"

Finally, clean up.

    FINALLY WEBAPP terminates
