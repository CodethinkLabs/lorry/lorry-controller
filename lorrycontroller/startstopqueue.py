# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import logging

import bottle

import lorrycontroller


class StartQueue(lorrycontroller.LorryControllerRoute):

    http_method = 'POST'
    path = '/1.0/start-queue'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)
        statedb = self.open_statedb()
        with statedb:
            statedb.set_running_queue(True)

        if 'redirect' in bottle.request.forms:
            bottle.redirect(bottle.request.forms.redirect)

        return 'Queue set to run'


class StopQueue(lorrycontroller.LorryControllerRoute):

    http_method = 'POST'
    path = '/1.0/stop-queue'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)
        statedb = self.open_statedb()
        with statedb:
            statedb.set_running_queue(False)

        if 'redirect' in bottle.request.forms:
            bottle.redirect(bottle.request.forms.redirect)

        return 'Queue set to not run'
