# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import logging
import time

import bottle

import lorrycontroller


class ListAllJobs(lorrycontroller.LorryControllerRoute):

    http_method = 'GET'
    path = '/1.0/list-jobs'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)

        statedb = self.open_statedb()
        return { 'job_ids': statedb.get_job_ids() }


class ListAllJobsHTML(lorrycontroller.LorryControllerRoute):

    http_method = 'GET'
    path = '/1.0/list-jobs-html'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)
        statedb = self.open_statedb()
        now = statedb.get_current_time()
        values = {
            'job_infos': self.get_jobs(statedb),
            'timestamp':
                time.strftime('%Y-%m-%d %H:%M:%S UTC', time.gmtime(now)),
            }
        return bottle.template(self._templates['list-jobs'], **values)

    def get_jobs(self, statedb):
        jobs = []
        for job_id, path, exit in statedb.get_all_jobs_id_path_exit():
            job = {
                'job_id': job_id,
                'exit': 'no' if exit is None else str(exit),
                'path': path,
                }
            jobs.append(job)
        return jobs
