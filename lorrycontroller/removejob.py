# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import logging

import bottle

import lorrycontroller


class RemoveJob(lorrycontroller.LorryControllerRoute):

    http_method = 'POST'
    path = '/1.0/remove-job'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)

        job_id = bottle.request.forms.job_id

        statedb = self.open_statedb()
        with statedb:
            try:
                statedb.find_lorry_running_job(job_id)
            except lorrycontroller.WrongNumberLorriesRunningJob:
                pass
            else:
                return { 'job_id': None, 'reason': 'still running' }

            statedb.remove_job(job_id)
            return { 'job_id': job_id }
