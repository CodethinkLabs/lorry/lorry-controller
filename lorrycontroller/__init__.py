# Copyright (C) 2014-2020  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from .statedb import (
    StateDB,
    LorryNotFoundError,
    WrongNumberLorriesRunningJob,
    HostNotFoundError)
from .route import LorryControllerRoute
from .readconf import ReadConfiguration
from .status import Status, StatusHTML, StatusRenderer, FailuresHTML
from .listqueue import ListQueue
from .showlorry import ShowLorry, ShowLorryHTML
from .startstopqueue import StartQueue, StopQueue
from .givemejob import GiveMeJob
from .jobupdate import JobUpdate
from .listrunningjobs import ListRunningJobs
from .movetopbottom import MoveToTop, MoveToBottom
from .stopjob import StopJob
from .listjobs import ListAllJobs, ListAllJobsHTML
from .showjob import ShowJob, ShowJobHTML, JobShower
from .removeghostjobs import RemoveGhostJobs
from .removejob import RemoveJob
from .lsupstreams import LsUpstreams, ForceLsUpstream
from .pretendtime import PretendTime
from .maxjobs import GetMaxJobs, SetMaxJobs
from .static import StaticFile
from .proxy import setup_proxy
from . import gerrit
from . import gitano
from . import gitea
from . import gitlab
from . import local


downstream_types = {
    'gerrit': gerrit.GerritDownstream,
    'gitano': gitano.GitanoDownstream,
    'gitea':  gitea.GiteaDownstream,
    'gitlab': gitlab.GitlabDownstream,
    'local':  local.LocalDownstream,
}


upstream_types = {
    'gitlab': gitlab.GitlabUpstream,
    'trove':  gitano.TroveUpstream,
}


def get_upstream_host(host_info):
    return upstream_types[host_info['type']](host_info)


__all__ = [
    'StateDB',
    'LorryNotFoundError', 'WrongNumberLorriesRunningJob', 'HostNotFoundError',
    'LorryControllerRoute',
    'ReadConfiguration',
    'Status', 'StatusHTML', 'StatusRenderer', 'FailuresHTML',
    'ListQueue',
    'ShowLorry', 'ShowLorryHTML',
    'StartQueue', 'StopQueue',
    'GiveMeJob',
    'JobUpdate',
    'ListRunningJobs',
    'MoveToTop', 'MoveToBottom',
    'StopJob',
    'ListAllJobs', 'ListAllJobsHTML',
    'ShowJob', 'ShowJobHTML', 'JobShower',
    'RemoveGhostJobs',
    'RemoveJob',
    'LsUpstreams', 'ForceLsUpstream',
    'PretendTime',
    'GetMaxJobs', 'SetMaxJobs',
    'StaticFile',
    'setup_proxy',
    'gerrit',
    'gitano',
    'gitea',
    'gitlab',
    'local',
    'downstream_types',
    'upstream_types',
    'get_upstream_host',
]
